#!/bin/bash

# script that Updates > Upgrades > Update the Databese.

clear

echo "Update ..."
sleep 5
sudo apt update -y

clear

echo "Upgrade ..."
sleep 5
sudo apt dist-upgrade -y

clear

echo "Cleaning ..."
sleep 3
sudo apt autoremove -y

echo "Update the Database ..."
sleep 3
sudo updatedb

echo "Done!"

sleep 5
exit 0

